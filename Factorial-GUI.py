import tkinter as tk
from tkinter import ttk

def calculate_factorial(n):
  if n > 20:
    return "No puc calcular el factorial de números majors que 20!"

  result = 1
  for i in range(1, n+1):
    result *= i

  return result

# Create the main mainframe
root = tk.Tk()
root.title("Factorial")
root.resizable(False, False)

mainframe = ttk.Frame(root, padding="3 3 12 12")
mainframe.grid(column=0, row=0)
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)

# Add a label and an entry box
label = tk.Label(mainframe, text="Introdueix un número:")
label.grid(row=0, column=0, pady=5)
entry = tk.Entry(mainframe)
entry.grid(row=0, column=1, pady=5)

# Add a button to calculate the factorial
button = tk.Button(mainframe, text="Calcula", command=lambda: calculate_and_show_result())
button.grid(row=1, column=1)

# Add a label to show the result
result_label = tk.Label(mainframe)
result_label.grid(row=2, column=1)

# Function to calculate the factorial and show the result
def calculate_and_show_result():
  # Get the input value
  n = int(entry.get())

  # Calculate the factorial
  result = calculate_factorial(n)

  # Check if the result is an error message
  if isinstance(result, str):
    result_label.config(text=result)
  else:
    result_label.config(text=f"{n}! = {result}")

# Run the main loop
mainframe.mainloop()